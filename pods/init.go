package pods

import (
	"message_broker/pods/logic"
	"message_broker/pods/repository"
	"message_broker/util"
)

func DefaultService() (*Service, error) {
	return CustomService(nil, nil, nil)
}

func CustomService(customLogic LogicImplementation, customDB logic.Repository, customRAM logic.Repository) (*Service, error) {
	if !util.IsNil(customLogic) {
		return &Service{customLogic}, nil
	}
	dbInjection := customDB
	ramInjection := customRAM

	if util.IsNil(customDB) {
		dbInjection = repository.NewDB()
	}
	if util.IsNil(customRAM) {
		ramInjection = repository.NewRAM()
	}
	l, e := logic.New(dbInjection, ramInjection)
	if e != nil {
		return nil, e
	}
	return &Service{l}, nil
}

func ApiFromService(s *Service) *Api {
	if util.IsNil(s) {
		return nil
	}
	return &Api{s.logic}
}
func CustomApi(customLogic LogicImplementation, customDB logic.Repository, customRAM logic.Repository) *Api {
	if !util.IsNil(customLogic) {
		return &Api{customLogic}
	}
	dbInjection := customDB
	ramInjection := customRAM

	if util.IsNil(customDB) {
		dbInjection = repository.NewDB()
	}
	if util.IsNil(customRAM) {
		ramInjection = repository.NewRAM()
	}
	l, e := logic.New(dbInjection, ramInjection)
	if e != nil {
		return nil
	}
	return &Api{l}
}
