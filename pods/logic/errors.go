package logic

import "github.com/pkg/errors"

var InvalidNameAddrSaveError = errors.New("You must provide a valid NAME and ADDRESS in order to register a new Pod")
var DbConnectionError = errors.New("Error connecting to database. See logs for details")
var NilPodError = errors.New("The Pod used as argument is nil")
var NilRepositoryNewLogicError = errors.New("You must provide both DB and RAM implementations")
