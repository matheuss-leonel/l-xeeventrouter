package tests

import (
	"message_broker/pods/logic"
	test2 "message_broker/util/testUtil"
	"testing"
)

func TestIsRegistered(t *testing.T) {
	mock := &mockRepository{}
	l, _ := logic.New(&mockRepository{}, mock)

	type test struct {
		input  string
		mock   logic.Pod
		expect bool
	}
	tests := []test{
		{"SomeRegisteredService", &fakePod{}, true},
		{"NonRegisteredService", nil, false},
		{"", nil, false},
	}

	for _, test := range tests {
		*mock = mockRepository{}
		mock.On("Fetch", test.input).Return(test.mock, nil)
		if l.IsRegistered(test.input) != test.expect {
			t.Fatal(test2.NotEqual("BOOL", test.expect, !test.expect))
		}
		mock.AssertNumberOfCalls(t, "Fetch", 1)
		mock.AssertExpectations(t)
	}
}

func TestOne(t *testing.T) {
	mockRAM := &mockRepository{}
	l, _ := logic.New(&mockRepository{}, mockRAM)

	expectedPod := &fakePod{}
	mockRAM.On("Fetch", "someServiceAddress").Return(expectedPod, nil)
	receiv := l.One("someServiceAddress")
	if receiv != expectedPod {
		t.Fatal(test2.NotEqual("POD", expectedPod, receiv))
	}
	mockRAM.AssertNumberOfCalls(t, "Fetch", 1)
	mockRAM.AssertExpectations(t)
}

func TestIterate(t *testing.T) {
	mockRAM := &mockRepository{}
	l, _ := logic.New(&mockRepository{}, mockRAM)

	type test struct {
		input orderingFunction
		mock  []logic.Pod
		expec []logic.Pod
		match func(i int, p logic.Pod) bool
	}
	unordered := fakeSlicePods()
	nameAsc, nameDesc := orderPods(unordered, podsNameAsc), orderPods(unordered, podsNameDesc)
	addrAsc, addrDesc := orderPods(unordered, podsAddressAsc), orderPods(unordered, podsAddressDesc)
	latAsc, latDesc := orderPods(unordered, podsLatencyAsc), orderPods(unordered, podsLatencyDesc)
	tests := []test{
		{nil, nil, nil, nil},
		{nil, unordered, unordered, func(i int, p logic.Pod) bool { return unordered[i].Address() == p.Address() }},
		{podsNameAsc, nil, nil, nil},
		{podsNameDesc, nil, nil, nil},
		{podsAddressAsc, nil, nil, nil},
		{podsAddressDesc, nil, nil, nil},
		{podsLatencyAsc, nil, nil, nil},
		{podsLatencyDesc, nil, nil, nil},
		{podsNameAsc, unordered, nameAsc, func(i int, p logic.Pod) bool { return nameAsc[i].Name() == p.Name() }},
		{podsNameDesc, unordered, nameDesc, func(i int, p logic.Pod) bool { return nameDesc[i].Name() == p.Name() }},
		{podsAddressAsc, unordered, addrAsc, func(i int, p logic.Pod) bool { return addrAsc[i].Address() == p.Address() }},
		{podsAddressDesc, unordered, addrDesc, func(i int, p logic.Pod) bool { return addrDesc[i].Address() == p.Address() }},
		{podsLatencyAsc, unordered, latAsc, func(i int, p logic.Pod) bool { return latAsc[i].Latency() == p.Latency() }},
		{podsLatencyDesc, unordered, latDesc, func(i int, p logic.Pod) bool { return latDesc[i].Latency() == p.Latency() }},
	}

	for _, test := range tests {
		*mockRAM = mockRepository{}
		mockRAM.On("FetchAll").Return(test.mock, nil)
		next := l.Iterate(test.input)
		i := 0
		for pod := next(); pod != nil; pod = next() {
			if test.match != nil && !test.match(i, pod) {
				t.Fatal(test2.NotEqual("POD", test.expec[i], pod))
			}
			i++
		}
		if i != len(test.expec) {
			t.Fatalf(test2.NotEqual("LENGTH", len(test.expec), i))
		}
		mockRAM.AssertNumberOfCalls(t, "FetchAll", 1)
		mockRAM.AssertExpectations(t)
	}
}

func TestIterateByLatency(t *testing.T) {
	mockRAM := &mockRepository{}
	l, _ := logic.New(&mockRepository{}, mockRAM)

	type test struct {
		mockOut  []logic.Pod
		expected []logic.Pod
	}
	unordered := fakeSlicePods()
	latencyAsc := orderPods(unordered, podsLatencyAsc)
	tests := []test{
		{nil, nil},
		{unordered, latencyAsc},
	}

	for _, test := range tests {
		*mockRAM = mockRepository{}
		mockRAM.On("FetchAll").Return(test.mockOut, nil)
		next := l.IterateByLatency()
		i := 0
		for pod := next(); pod != nil; pod = next() {
			if pod != test.expected[i] {
				t.Fatalf(test2.NotEqual("POD", test.expected[i], pod))
			}
			i++
		}
		if i != len(test.expected) {
			t.Fatalf(test2.NotEqual("LENGTH", len(test.expected), i))
		}
		mockRAM.AssertNumberOfCalls(t, "FetchAll", 1)
		mockRAM.AssertExpectations(t)
	}
}

func TestIterateCluster(t *testing.T) {
	mockRAM := &mockRepository{}
	l, _ := logic.New(&mockRepository{}, mockRAM)

	type test struct {
		mockInName string
		mockInCmp  orderingFunction
		mockOut    []logic.Pod
		expected   []logic.Pod
	}
	unordered := fakeSlicePods()
	clusters := podsByCluster(unordered)
	tests := make([]test, len(clusters)*7+8)
	i := 0
	for name, cluster := range clusters {
		tests[i] = test{name, podsNameAsc, unordered, orderPods(cluster, podsNameAsc)}
		tests[i+1] = test{name, podsNameDesc, unordered, orderPods(cluster, podsNameDesc)}
		tests[i+2] = test{name, podsAddressAsc, unordered, orderPods(cluster, podsAddressAsc)}
		tests[i+3] = test{name, podsAddressDesc, unordered, orderPods(cluster, podsAddressDesc)}
		tests[i+4] = test{name, podsLatencyAsc, unordered, orderPods(cluster, podsLatencyAsc)}
		tests[i+5] = test{name, podsLatencyDesc, unordered, orderPods(cluster, podsLatencyDesc)}
		tests[i+6] = test{name, nil, unordered, cluster}
		i += 7
	}
	tests[i] = test{"", nil, nil, nil}
	tests[i+1] = test{"", nil, unordered, nil}
	tests[i+2] = test{"", podsNameDesc, nil, nil}
	tests[i+3] = test{"", podsNameAsc, unordered, nil}
	tests[i+4] = test{"NonexistentService", podsNameAsc, nil, nil}
	tests[i+5] = test{"NonexistentService", nil, unordered, nil}
	tests[i+6] = test{"NonexistentService", podsNameAsc, unordered, nil}
	tests[i+7] = test{"NonexistentService", nil, nil, nil}

	for _, test := range tests {
		*mockRAM = mockRepository{}
		mockRAM.On("FetchAll").Return(test.mockOut, nil)
		next := l.IterateCluster(test.mockInName, test.mockInCmp)
		i := 0
		for pod := next(); pod != nil; pod = next() {
			if pod != test.expected[i] {
				t.Fatal(test2.NotEqual("POD", test.expected[i], pod))
			}
			i++
		}
		if i != len(test.expected) {
			t.Fatal(test2.NotEqual("LENGTH", len(test.expected), i))
		}
		mockRAM.AssertNumberOfCalls(t, "FetchAll", 1)
		mockRAM.AssertExpectations(t)
	}
}

func TestIterateClusterByLatency(t *testing.T) {
	mockRAM := &mockRepository{}
	l, _ := logic.New(&mockRepository{}, mockRAM)

	type test struct {
		mockIn   string
		mockOut  []logic.Pod
		expected []logic.Pod
	}
	unordered := fakeSlicePods()
	clusters := podsByCluster(unordered)
	tests := make([]test, len(clusters)*2+4)
	i := 0
	for name, cluster := range clusters {
		tests[i] = test{name, unordered, orderPods(cluster, podsLatencyAsc)}
		tests[i+1] = test{name, nil, nil}
		i += 2
	}
	tests[i] = test{"", nil, nil}
	tests[i+1] = test{"", unordered, nil}
	tests[i+2] = test{"NonexistentService", nil, nil}
	tests[i+3] = test{"NonexistentService", unordered, nil}

	for _, test := range tests {
		*mockRAM = mockRepository{}
		mockRAM.On("FetchAll").Return(test.mockOut, nil)
		next := l.IterateClusterByLatency(test.mockIn)
		i := 0
		for pod := next(); pod != nil; pod = next() {
			if pod != test.expected[i] {
				t.Fatal(test2.NotEqual("POD", test.expected[i], pod))
			}
			i++
		}
		if i != len(test.expected) {
			t.Fatal(test2.NotEqual("LENGTH", len(test.expected), i))
		}
		mockRAM.AssertNumberOfCalls(t, "FetchAll", 1)
		mockRAM.AssertExpectations(t)
	}
}
