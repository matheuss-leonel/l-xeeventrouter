package tests

import (
	"math/rand"
	"message_broker/pods/logic"
	"sort"
	"time"
)

func fakeSlicePods() []logic.Pod {
	rand.Seed(time.Now().UnixNano())
	serviceAmount := (rand.Int() % 10) + 10
	podAmount := (rand.Int() % 10) + 10
	pods := make([]logic.Pod, podAmount*serviceAmount)
	for i := 0; i < serviceAmount; i++ {
		name := (rand.Int() % 26) + 65
		for j := 0; j < podAmount; j++ {
			addr := (rand.Int() % 26) + 65
			lat := (rand.Int() % 26) + 65
			pods[i*podAmount+j] = &fakePod{
				name:    string(name) + "_Service",
				address: string(addr) + "_Address",
				latency: uint64(lat),
			}
		}
	}
	return pods
}

func podsByCluster(pods []logic.Pod) map[string][]logic.Pod {
	mapped := make(map[string][]logic.Pod)
	for _, pod := range pods {
		mapped[pod.Name()] = append(mapped[pod.Name()], pod)
	}
	return mapped
}

func orderPods(pods []logic.Pod, cmp orderingFunction) []logic.Pod {
	newPods := make([]logic.Pod, len(pods))
	copy(newPods, pods)
	sort.Slice(newPods, func(i, j int) bool {
		return cmp(newPods[i], newPods[j])
	})
	return newPods
}

type orderingFunction func(a, b logic.Pod) bool

var podsNameAsc orderingFunction = func(a, b logic.Pod) bool { return a.Name() < b.Name() }
var podsNameDesc orderingFunction = func(a, b logic.Pod) bool { return a.Name() > b.Name() }
var podsAddressAsc orderingFunction = func(a, b logic.Pod) bool { return a.Address() < b.Address() }
var podsAddressDesc orderingFunction = func(a, b logic.Pod) bool { return a.Address() > b.Address() }
var podsLatencyAsc orderingFunction = func(a, b logic.Pod) bool { return a.Latency() < b.Latency() }
var podsLatencyDesc orderingFunction = func(a, b logic.Pod) bool { return a.Latency() > b.Latency() }
