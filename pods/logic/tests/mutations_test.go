package tests

import (
	"errors"
	"message_broker/pods/logic"
	test2 "message_broker/util/testUtil"
	"testing"
)

func TestRegister(t *testing.T) {
	mockRAM := &mockRepository{}
	mockDB := &mockRepository{}
	l, _ := logic.New(mockDB, mockRAM)

	type mock struct {
		fetchRAM  logic.Pod
		saveRAM   error
		savePodDB logic.Pod
		saveErrDB error
	}
	type test struct {
		inputName string
		inputAddr string
		mocks     mock
		expectPod logic.Pod
		expectErr error
	}
	alreadyRegistered := mock{&fakePod{}, nil, nil, nil}
	success := mock{nil, nil, &fakePod{}, nil}
	dbSaveError := mock{nil, nil, nil, errors.New("")}
	tests := []test{
		{"", "", mock{}, nil, logic.InvalidNameAddrSaveError},
		{"", "invalidURL", mock{}, nil, logic.InvalidNameAddrSaveError},
		{"", "http://some.url", mock{}, nil, logic.InvalidNameAddrSaveError},
		{"ServiceName", "", mock{}, nil, logic.InvalidNameAddrSaveError},
		{"ServiceName", "invalidURL", mock{}, nil, logic.InvalidNameAddrSaveError},
		{"ServiceName", "invalidURL", mock{}, nil, logic.InvalidNameAddrSaveError},
		{"ServiceName", "http://some.url", alreadyRegistered, alreadyRegistered.fetchRAM, nil},
		{"ServiceName", "http://some.url", success, success.savePodDB, nil},
		{"ServiceName", "http://some.url/any/thing", success, success.savePodDB, nil},
		{"ServiceName", "http://some.url/any/thing?var=1", success, success.savePodDB, nil},
		{"ServiceName", "http://some.url/any/thing?var=1&var2=3", success, success.savePodDB, nil},
		{"ServiceName", "http://some.url/any/thing#slug", success, success.savePodDB, nil},
		{"ServiceName", "http://some.url", dbSaveError, nil, logic.DbConnectionError},
	}

	for _, test := range tests {
		*mockRAM = mockRepository{}
		*mockDB = mockRepository{}
		mockRAM.On("Fetch", test.inputAddr).Return(test.mocks.fetchRAM, nil)
		mockRAM.On("Save", test.inputName, test.inputAddr).Return(nil, test.mocks.saveRAM)
		mockDB.On("Save", test.inputName, test.inputAddr).Return(test.mocks.savePodDB, test.mocks.saveErrDB)
		pod, err := l.Register(test.inputName, test.inputAddr)
		if pod != test.expectPod {
			t.Fatal(test2.NotEqual("POD", test.expectPod, pod))
		}
		if err != test.expectErr {
			t.Fatal(test2.NotEqual("ERROR", test.expectErr, err))
		}
	}
}

func TestUnregister(t *testing.T) {
	mockRAM := &mockRepository{}
	mockDB := &mockRepository{}
	l, _ := logic.New(mockDB, mockRAM)

	type mock struct {
		ramDelete error
		dbDelete  error
	}
	type test struct {
		input  logic.Pod
		mocks  mock
		expect error
	}
	dbDeleteError := mock{nil, errors.New("")}
	noMockErrors := mock{nil, nil}
	tests := []test{
		{nil, noMockErrors, logic.NilPodError},
		{&fakePod{address: "addr"}, dbDeleteError, logic.DbConnectionError},
		{&fakePod{address: "addr"}, noMockErrors, nil},
	}

	for _, test := range tests {
		*mockRAM = mockRepository{}
		*mockDB = mockRepository{}
		if test.input != nil {
			mockRAM.On("Delete", test.input.Address()).Return(test.mocks.ramDelete)
			mockDB.On("Delete", test.input.Address()).Return(test.mocks.dbDelete)
		}
		err := l.Unregister(test.input)
		if err != test.expect {
			t.Fatalf(test2.NotEqual("ERROR", test.expect, err))
		}
	}
}

func TestKill(t *testing.T) {
	l, _ := logic.New(&mockRepository{}, &mockRepository{})

	type test struct {
		input  logic.Pod
		expect error
	}
	mockedMutex := &mockMutex{}
	tests := []test{
		{nil, logic.NilPodError},
		{&fakePod{zombie: false, mutex: mockedMutex}, nil},
		{&fakePod{zombie: true, mutex: mockedMutex}, nil},
	}

	for _, test := range tests {
		mockedMutex.On("Lock")
		mockedMutex.On("Unlock")
		err := l.Kill(test.input)
		if err != test.expect {
			t.Fatalf(test2.NotEqual("ERROR", test.expect, err))
		}
		if test.input != nil && !test.input.Zombie() {
			t.Fatalf(test2.NotEqual("ZOMBIE", !test.input.Zombie(), test.input.Zombie()))
		}
		// Ensure that the operation is thread-safe
		if test.input != nil {
			mockedMutex.AssertNumberOfCalls(t, "Lock", 1)
			mockedMutex.AssertNumberOfCalls(t, "Unlock", 1)
		}
		*mockedMutex = mockMutex{}
	}
}

func TestWakeup(t *testing.T) {
	l, _ := logic.New(&mockRepository{}, &mockRepository{})

	type test struct {
		input  logic.Pod
		expect error
	}
	mockedMutex := &mockMutex{}
	tests := []test{
		{nil, logic.NilPodError},
		{&fakePod{zombie: false, mutex: mockedMutex}, nil},
		{&fakePod{zombie: true, mutex: mockedMutex}, nil},
	}

	for _, test := range tests {
		mockedMutex.On("Lock")
		mockedMutex.On("Unlock")
		err := l.Wakeup(test.input)
		if err != test.expect {
			t.Fatalf(test2.NotEqual("ERROR", test.expect, err))
		}
		if test.input != nil && test.input.Zombie() {
			t.Fatalf(test2.NotEqual("ZOMBIE", test.input.Zombie(), !test.input.Zombie()))
		}
		// Ensure that the operation is thread-safe
		if test.input != nil {
			mockedMutex.AssertNumberOfCalls(t, "Lock", 1)
			mockedMutex.AssertNumberOfCalls(t, "Unlock", 1)
		}
		*mockedMutex = mockMutex{}
	}
}

func TestSetLatency(t *testing.T) {
	l, _ := logic.New(&mockRepository{}, &mockRepository{})

	type test struct {
		inputPod logic.Pod
		inputLat uint64
		expecOld uint64
		expecErr error
	}
	mockedMutex := &mockMutex{}
	tests := []test{
		{nil, 0, 0, logic.NilPodError},
		{&fakePod{latency: 10, mutex: mockedMutex}, 250, 10, nil},
		{&fakePod{latency: 0, mutex: mockedMutex}, 115, 0, nil},
	}

	for _, test := range tests {
		mockedMutex.On("Lock")
		mockedMutex.On("Unlock")
		oldLat, err := l.SetLatency(test.inputPod, test.inputLat)

		if test.inputPod != nil {
			if test.inputPod.Latency() != test.inputLat {
				t.Fatalf(test2.NotEqual("LATENCY", test.inputLat, test.inputPod.Latency()))
			}
			// Ensure that the operation is thread-safe
			mockedMutex.AssertNumberOfCalls(t, "Lock", 1)
			mockedMutex.AssertNumberOfCalls(t, "Unlock", 1)
		}
		if oldLat != test.expecOld {
			t.Fatalf(test2.NotEqual("LATENCY", test.expecOld, oldLat))
		}
		if err != test.expecErr {
			t.Fatalf(test2.NotEqual("ERROR", test.expecErr, err))
		}
		*mockedMutex = mockMutex{}
	}
}
