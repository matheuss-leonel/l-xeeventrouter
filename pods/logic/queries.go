package logic

import (
	"sort"
)

// podExists will verify if the given address
// belong to any Worker saved at the db
func podExists(address string) bool {
	// 1. RAM dont throw errors
	pod, _ := ram.Fetch(address)
	if pod != nil {
		return true
	}
	return false
}

// fetchOne will just try to retrieve the Worker from the
// ram
func fetchOne(address string) Pod {
	// 1. RAM dont throw errors
	pod, _ := ram.Fetch(address)
	return pod
}

// fetchCluster will filter only the workers that matches
// the clusterName provided, sorting it if a CMP function
// is provided and returning a closure to get it's content
//
// The order which the workers are returned depends on the
// sort algorithm used
func fetchCluster(clusterName string, cmp func(a, b Pod) bool) func() Pod {
	// TODO: Optimize this operation
	// 1. Filter the original slice by the Name
	var cluster []Pod
	all, _ := ram.FetchAll()
	for _, w := range all {
		if w.Name() == clusterName {
			cluster = append(cluster, w)
		}
	}

	// 2. Sort if the function is provided
	if cmp != nil {
		sort.Slice(cluster, func(i, j int) bool {
			return cmp(cluster[i], cluster[j])
		})
	}

	// 3. Return the closure
	i := 0
	return func() Pod {
		if i >= len(cluster) {
			return nil
		}
		next := cluster[i]
		i++
		return next
	}
}

// fetchAll will get a function to sort the workers (if provided)
// and will return a closure that return the next Pod when
// called.
//
// The order which the workers are returned depends on the
// sort algorithm used
func fetchAll(cmp func(a, b Pod) bool) func() Pod {
	// 1. It's already a copy
	all, _ := ram.FetchAll()

	// 2. Sort according to the given CMP function, if provided
	if cmp != nil {
		sort.Slice(all, func(i, j int) bool {
			return cmp(all[i], all[j])
		})
	}

	// 3. Return a closure
	i := 0
	return func() Pod {
		if i >= len(all) {
			return nil
		}
		next := all[i]
		i++
		return next
	}
}
