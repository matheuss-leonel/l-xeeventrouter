package logic

import "sync"

// Pod represents all the functions that a
// Pod must implement to be correctly used
// by the logic layer
type Pod interface {
	Name() string
	Address() string
	Latency() uint64
	Zombie() bool
	Mutex() sync.Locker
	SetZombie(bool)
	SetLatency(uint64)
}

