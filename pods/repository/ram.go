package repository

import (
	"sync"
)

// TODO: encapsular isso aqui dentro de uma struct, pra gerar varias instancias, se necessario
var hostStore []*host
var mutex = &sync.Mutex{}

// createAndSave will create a new instance of host and
// add it to it's cluster list
func createAndSave(name string, address string) *host {
	newOne := newHost(name, address)
	mutex.Lock()
	hostStore = append(hostStore, newOne)
	mutex.Unlock()
	return newOne
}

// remove will get an address of a host and remove it
// from the store.
//
// If none match the given string, it's just a noop.
func remove(address string) error {
	mutex.Lock()
	defer mutex.Unlock()

	for i, h := range hostStore {
		if h.address == address {
			hostStore = append(hostStore[:i], hostStore[i+1:]...)
			return nil
		}
	}

	return nil
}

// fetch will return the host that matches
// the given address
func fetch(address string) *host {
	mutex.Lock()
	defer mutex.Unlock()

	for _, h := range hostStore {
		if h.address == address {
			return h
		}
	}
	return nil
}

// fetchAll will make a local copy of
// the store and return a closure that
// when called return the next host in
// the list.
//
// When the list reaches the end, the
// closure will return nil value
func fetchAll() []*host {
	localStore := hostStore
	return localStore
}

// count will return the length of
// the hostStore
func count() uint64 {
	return uint64(len(hostStore))
}
