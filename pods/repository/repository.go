package repository

import (
	"message_broker/pods/logic"
)

// db is the direct communication with the
// database used to store the Workers
type db struct{}

func (db *db) Save(name string, address string) (logic.Pod, error) {
	return nil, nil
}
func (db *db) Delete(address string) error {
	return nil
}
func (db *db) Fetch(address string) (logic.Pod, error) { return nil, nil }
func (db *db) FetchAll() ([]logic.Pod, error)          { return nil, nil }
func (db *db) Count() (uint64, error)                  { return 0, nil }

// ram is used to store the data in-memory,
// to prevent the use of the database
type ram struct{}

func (r *ram) Save(name string, address string) (logic.Pod, error) {
	return createAndSave(name, address), nil
}
func (r *ram) Delete(address string) error {
	return remove(address)
}
func (r *ram) Fetch(address string) (logic.Pod, error) {
	return fetch(address), nil
}
func (r *ram) FetchAll() ([]logic.Pod, error) {
	store := fetchAll()
	pods := make([]logic.Pod, len(store))
	for i, host := range store {
		pods[i] = host
	}
	return pods, nil
}
func (r *ram) Count() (uint64, error) {
	return count(), nil
}
