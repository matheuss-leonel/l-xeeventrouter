package webserver

import (
	"message_broker/log"
	"message_broker/network"

	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func StartUp(address string) {
	r := mux.NewRouter()
	initializeRoutes(r)
	log.LogInfo("Listening on address %s", address)
	srv := &http.Server{
		Handler: r,
		Addr: address,
		WriteTimeout: 15 * time.Second,
		ReadTimeout: 15 * time.Second,
	}
	go network.InitHealthChecker()
	srv.ListenAndServe()
}
