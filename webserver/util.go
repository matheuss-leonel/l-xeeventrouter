package webserver

import (
	"message_broker/log"
	server "net/http"
)

func asyncEndpoint(callback func(w server.ResponseWriter, r *server.Request)) (fn func(w server.ResponseWriter, r *server.Request)) {
	// 0. Handler just throws a goroutine
	return func(w server.ResponseWriter, r *server.Request) {
		// 1. Goroutine to do the hard work asynchronously
		go func() {
			// 2. Defer someone to recover from any exception
			defer func() {
				if r := recover(); r != nil {
					log.LogError("%v", r)
				}
			}()
			// 3. Call the final handler
			callback(w, r)
		}()
		// 4. Always return a 200 status code and return immediately
		w.WriteHeader(200)
		return
	}
}
