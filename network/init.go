package network

import (
	"message_broker/network/logic"
	"message_broker/util"
)

type Service interface {
	PerformRequest(addr string, evt string, data interface{}) error
}

func DefaultService(podStore logic.PodStore) (Service, error) {
	return logic.New(logic.Props{
		Log:      nil,
		Util:     &util.Util{},
		PodStore: podStore,
	})
}
