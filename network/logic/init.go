package logic

import "errors"

type Props struct {
	Log      log
	Util     util
	PodStore PodStore
}

func New(props Props) (*coreLogic, error) {
	if props.Util == nil || props.Log == nil || props.PodStore == nil {
		return nil, errors.New("you must provide all props")
	}
	return &coreLogic{props.Util, props.Log, props.PodStore}, nil
}
