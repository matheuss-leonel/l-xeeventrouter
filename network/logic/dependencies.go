package logic

type worker interface {
	Name() string
	Address() string
	Zombie() bool
}

type PodStore interface {
	Wakeup(addr string) error
	Kill(address string) error
	SetLatency(address string, newLatency uint64)
	IterateByLatency() func() worker
	Unregister(w worker) error
}

type log interface {
	Info(m string, i ...interface{})
	Error(m string, i ...interface{})
	Warning(m string, i ...interface{})
}

type util interface {
	Err(code int, msg string, i ...interface{}) error
	IsNil(i interface{}) bool
}
