package logic

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"
)

type coreLogic struct {
	util
	log
	PodStore
}

func (c *coreLogic) PerformRequest(addr string, evt string, data interface{}) error {
	// 0. Invalid argument errors
	if addr == "" || c.util.IsNil(evt) {
		return c.util.Err(InvalidArgumentsErr, "You must provide non nil values for both arguments (worker and event)")
	}

	// 2. Serialize the request data according to the requestContract
	jsonReq, e := json.Marshal(requestContract{
		Data:  data,
		Event: evt,
	})

	// 2.1. Inform serialization errors
	if e != nil {
		return c.util.Err(ReqContractSerializationErr, "An error occurred when serializing the [%s] event: %w", evt, e)
	}

	// 3. Perform the request and verify the latency
	t, e := c.timedRequest(addr + "/loxe", bytes.NewBuffer(jsonReq))

	// 4. Handle possible errors
	if e != nil {
		killErr := c.PodStore.Kill(addr)
		if killErr != nil {
			c.log.Error("Error occurred when killing [unknown:%s]: %v", addr, killErr)
		}
		return c.util.Err(UnreachableHostErr, "The target host is currently unreachable: %w", e)
	}

	// 5. Update the Worker latency time
	c.PodStore.SetLatency(addr, t)

	// 6. Nil error represents a success
	return nil
}

// requestContract represents the serialization that the WorkerInstances
// should support
type requestContract struct {
	Data		interface{}		`json:"data"`
	Event		string			`json:"event"`
}

// timedRequest will perform a request and track the time to answer arrives.
func (c *coreLogic) timedRequest(address string, data io.Reader) (t uint64, e error) {
	// 1. Set a timeout to the request. 1 second is ok
	client := http.Client{ Timeout: time.Second * 1 }

	// 2. Start a timer to check the response time
	start := time.Now()

	// 3. Perform the request
	res, e := client.Post(address, "application/json", data)

	// 4. Track request time
	t = uint64(time.Since(start).Nanoseconds())

	// 5.1. Return the error from the "Post" method
	if e != nil {
		return 0, e
	}

	// 5.2. Return an error because the status isn't 200
	if res.StatusCode != 200 {
		return 0, c.util.Err(Non200StatusCodeErr, "The request received a non 200 HTTP Status code: [%s]", string(res.StatusCode))
	}

	// 6. Return the time elapsed
	return t, nil
}

// healthChecker is responsible for ensure that the registered WorkerInstances are
// up and running
//
// IMPORTANT: this function can apply the following side-effects:
// 		1. Update WorkerInstance latency time
//		2. Put the WorkerInstance in the Zombie state
//		3. Unregister the WorkerInstance (Case it is not responding)
//		4. Wake up a Zombie WorkerInstance (The only way to wakeup someone)
func (c *coreLogic) healthChecker() {
	// 1. This goroutine will run forever
	for {
		// 2. It will sleep for 1 minute
		time.Sleep(time.Minute * 1)

		// 3. Inform via log that the operation started
		c.log.Info("Verifying the network consistency...")

		// 4. Iterate over the pods
		closure := c.PodStore.IterateByLatency()
		for w := closure(); w != nil; w = closure() {
				// 6. Perform the request to the WorkerInstance
				t, e := c.timedRequest(w.Address() + "/health", nil)

				// 7. Kill the worker if an error has occurred
				if e != nil {
					// 7.1. If it is already dead, unregister it
					if w.Zombie() {
						c.log.Warning("The [%s:%s] WorkerInstance is not responding. Removing it after a double fault", w.Name(), w.Address())
						unregErr := c.PodStore.Unregister(w)
						if unregErr != nil {
							c.log.Error("Error occurred when unregistering [%s:%s]: %v", w.Name(), w.Address(), unregErr)
						}
						continue
					}
					killErr := c.PodStore.Kill(w.Address())
					if killErr != nil {
						c.log.Error("Error occurred when killing [%s:%s]: %v", w.Name(), w.Address(), killErr)
					}
					continue
				}

				// 8. Without errors, wakeup the dead
				if w.Zombie() {
					wakeErr := c.PodStore.Wakeup(w.Address())
					if wakeErr != nil {
						c.log.Error("Error occurred when waking up [%s:%s]: %v", w.Name(), w.Address(), wakeErr)
					}
				}

				// 9. Update worker latency time
				c.PodStore.SetLatency(w.Address(), t)
		}
	}
}
