package main

import (
	"message_broker/log"
	"message_broker/webserver"
	"os"
)

func injectDependencies() {

}

func main() {
	defer func() {
	}()

	args := os.Args[1:]
	if len(args) < 1 {
		log.LogError("You must provide the address of the webserver (default: http://localhost:3000)")
		return
	}
	webserver.StartUp(args[0])
}
