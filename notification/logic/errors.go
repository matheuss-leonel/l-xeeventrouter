package logic

const (
	ChannelStoreCallErr = iota
	PublicationError
	UnrecognizedEvtType
)
