package testUtil

import (
	"github.com/stretchr/testify/mock"
	"reflect"
	"testing"
)

type TestSuite struct {
	T *testing.T
}

func (s *TestSuite) LogMock() *mockLogger { return &mockLogger{} }
func (s *TestSuite) UtilMock() *mockUtil  { return &mockUtil{} }
func (s *TestSuite) IsNil(i interface{}) bool {
	return i == nil || (reflect.ValueOf(i).Kind() == reflect.Ptr && reflect.ValueOf(i).IsNil())
}
func (s *TestSuite) NotEqual(expected, b interface{}) {
	s.T.Fatalf("Expectations not met:\n"+
		"                    Expected: %s\n"+
		"                    Received: %s", expected, b)
}

// ---

type mockLogger struct {
	mock.Mock
}

func (m *mockLogger) Error(s string, v ...interface{})   { m.Called(s, v) }
func (m *mockLogger) Info(s string, v ...interface{})    { m.Called(s, v) }
func (m *mockLogger) Warning(s string, v ...interface{}) { m.Called(s, v) }

// ---

type mockUtil struct {
	mock.Mock
}

func (m *mockUtil) Err(code int, msg string, i ...interface{}) error {
	args := m.Called(code, msg, i)
	return args.Error(0)
}
