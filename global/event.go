package global

// event struct contains all the necessary information to identify
// and debug the event.
type Event struct {
	Id			uint64
	Name      	string      /* Binds with the event queue map index				*/
	Publisher 	*Worker    /* Contains information about the publisher service	*/
	Data      	interface{} /* The data to be passed via request				*/
	Date      	uint64      /* Snapshot of the time when the event ocurred (ms)	*/
}

// The possible event types

type E interface {
}
