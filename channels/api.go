package channels

import (
	"message_broker/util"
	"net/http"
)

type endpoints struct {
	channelsService service
	clusterStore    clusterStore
}

// SubscribeEndpoint will handle all the requests that try to subscribe
// a already registered WorkerInstance to a new Event
func (a *endpoints) Subscribe(w http.ResponseWriter, r *http.Request) {
	// 1. Deserialize the POST request expected arguments
	var reqArgs struct {
		Name  string `json:"name"`
		Event string `json:"event"`
	}
	util.Deserialize(r, &reqArgs)

	// 2. Throw error case there's no WorkerInstance with this address
	if !a.clusterStore.ClusterExists(reqArgs.Name) {
		panic(util.NewErrorf("There's no [%s] Cluster registered", reqArgs.Name))
		return
	}

	// 4. Subscribe it to the given event
	e := a.channelsService.Subscribe(reqArgs.Event, reqArgs.Name)
	if e != nil {
		panic(e)
	}
}

// UnsubscribeEndpoint will handle all the requests that try to unsubscribe
// a registered WorkerInstance from the given event
func (a *endpoints) Unsubscribe(w http.ResponseWriter, r *http.Request) {
	// 1. Deserialize the POST request
	var reqArgs struct {
		Name  string `json:"name"`
		Event string `json:"event"`
	}
	util.Deserialize(r, &reqArgs)

	// 3. Throw error case there's no WorkerInstance with this address
	if !a.clusterStore.ClusterExists(reqArgs.Name) {
		panic(util.NewErrorf("There's no [%s] Cluster registered", reqArgs.Name))
		return
	}

	// 4. Unsubscribe from the given event
	e := a.channelsService.Unsubscribe(reqArgs.Event, reqArgs.Name)
	if e != nil {
		panic(e)
	}
}
