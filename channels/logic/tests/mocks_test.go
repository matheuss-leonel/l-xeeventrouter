package tests

import "github.com/stretchr/testify/mock"

type mockStore struct {
	mock.Mock
}

func (m *mockStore) Save(a, b string) error {
	args := m.Called(a, b)
	return args.Error(0)
}
func (m *mockStore) Delete(a, b string) error {
	args := m.Called(a, b)
	return args.Error(0)
}
func (m *mockStore) Fetch(a string) ([]string, error) {
	args := m.Called(a)
	slice := args.Get(0)
	if slice == nil {
		return nil, args.Error(1)
	}
	return slice.([]string), args.Error(1)
}
