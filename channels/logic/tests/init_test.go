package tests

import (
	"message_broker/channels/logic"
	test2 "message_broker/util/testUtil"
	"testing"
)

func TestNew(t *testing.T) {
	suite := &test2.TestSuite{t}

	noStore := logic.Props{nil, suite.LogMock(), suite.UtilMock()}
	noLOG:= logic.Props{&mockStore{}, nil, suite.UtilMock()}
	noUtil := logic.Props{&mockStore{}, suite.LogMock(), nil}
	okProps := logic.Props{&mockStore{}, suite.LogMock(), suite.UtilMock()}

	type test struct {
		props    logic.Props
		hasLogic bool
		hasErr   bool
	}
	tests := []test{
		{noStore, false, true},
		{noUtil, false, true},
		{noLOG, false, true},
		{okProps, true, false},
	}

	for _, test := range tests {
		l, e := logic.New(test.props)
		if test.hasErr && e == nil {
			t.Fatal("An error was expected")
		}
		if test.hasLogic && l == nil {
			t.Fatal("A properly created logic was expected")
		}
	}
}
