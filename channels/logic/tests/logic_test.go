package tests

import (
	"errors"
	"github.com/stretchr/testify/mock"
	"message_broker/channels/logic"
	"message_broker/util/testUtil"
	"testing"
)

func TestSubscribe(t *testing.T) {
	suite := &testUtil.TestSuite{}
	storeInitialState := map[string][]string{"ExistentEvent": {"ExistentClusterA", "ExistentClusterB"}}

	mockedStore := &mockStore{}
	mockedLog := suite.LogMock()
	mockedUtil := suite.UtilMock()

	l, e := logic.New(logic.Props{mockedStore, mockedLog, mockedUtil})
	if e != nil {
		t.Fatal("Error creating a valid logic core in order to testUtil")
		return
	}

	sentinelErr := errors.New("used to simulate store errors")

	infoMock := func(e, c string) { mockedLog.On("Info", mock.Anything, []interface{}{c, e}) }
	warnMock := func(e, c string) { mockedLog.On("Warning", mock.Anything, []interface{}{c, e}) }
	fetchErr := func(evt, cluster string) { mockedStore.On("Fetch", evt).Return(nil, sentinelErr) }
	saveErr := func(evt, cluster string) { mockedStore.On("Save", evt, cluster).Return(sentinelErr) }
	invalidArg := func(evt, cluster string) {
		mockedUtil.On("Err", logic.InvalidArgumentsErr, mock.Anything, []interface{}{evt, cluster}).Return(sentinelErr)
	}
	storeFetchErr := func(evt, cluster string) {
		mockedUtil.On("Err", logic.StoreFetchErr, mock.Anything, []interface{}{evt, sentinelErr}).Return(sentinelErr)
	}
	storeNonErr := func(e, c string) {
		mockedStore.On("Fetch", e).Return(nil, nil)
		mockedStore.On("Save", e, c).Return(nil)
	}
	storeSaveErr := func(evt, cluster string) {
		mockedStore.On("Fetch", evt).Return(storeInitialState[evt], nil)
		mockedUtil.On("Err", logic.StoreSaveErr, mock.Anything, []interface{}{cluster, evt, sentinelErr}).Return(sentinelErr)
	}
	storeAlreadyExistent := func(e, c string) {
		mockedStore.On("Fetch", e).Return(storeInitialState[e], nil)
	}

	type test struct {
		inputEvt     string
		inputCluster string
		errMock      func(evt, cluster string)
		storeMock    func(evt, cluster string)
		logMock      func(evt, cluster string)
	}
	tests := []test{
		// Input errors
		{"", "", invalidArg, nil, nil},
		{"", "AnyCluster", invalidArg, nil, nil},
		{"AnyEvt", "", invalidArg, nil, nil},

		// Persistence errors
		{"FetchError", "FetchError", storeFetchErr, fetchErr, nil},
		{"SaveError", "SaveError", storeSaveErr, saveErr, nil},

		// Success
		{"NewEvent", "NewCluster", nil, storeNonErr, infoMock},
		{"NewEvent", "ExistentClusterA", nil, storeNonErr, infoMock},
		{"ExistentEvent", "NewCluster", nil, storeNonErr, infoMock},
		{"ExistentEvent", "ExistentClusterB", nil, storeAlreadyExistent, warnMock},
	}

	for _, test := range tests {
		if test.errMock != nil {
			test.errMock(test.inputEvt, test.inputCluster)
		}
		if test.storeMock != nil {
			test.storeMock(test.inputEvt, test.inputCluster)
		}
		if test.logMock != nil {
			test.logMock(test.inputEvt, test.inputCluster)
		}

		e := l.Subscribe(test.inputEvt, test.inputCluster)

		if test.errMock == nil && e != nil {
			suite.NotEqual("nil", e)
		}

		if test.logMock != nil {
			mockedLog.AssertExpectations(t)
			*mockedLog = *suite.LogMock()
		}
		if test.errMock != nil {
			mockedUtil.AssertExpectations(t)
			*mockedUtil = *suite.UtilMock()
		}
		if test.storeMock != nil {
			mockedStore.AssertExpectations(t)
			*mockedStore = mockStore{}
		}
	}
}

func TestUnsubscribe(t *testing.T) {
	suite := &test2.TestSuite{}

	mockedStore := &mockStore{}
	mockedLog := suite.LogMock()
	mockedUtil := suite.UtilMock()

	l, e := logic.New(logic.Props{mockedStore, mockedLog, mockedUtil})
	if e != nil {
		t.Fatal("Error creating a valid logic core in order to testUtil")
		return
	}

	sentinelErr := errors.New("used to simulate store errors")
	invalidArg := func(evt, cluster string) {
		mockedUtil.On("Err", logic.InvalidArgumentsErr, mock.Anything, []interface{}{evt, cluster}).Return(sentinelErr)
	}
	deleteErr := func(evt, cluster string) {
		mockedStore.On("Delete", evt, cluster).Return(sentinelErr)
	}
	storeDeleteErr := func(evt, cluster string) {
		mockedUtil.On("Err", logic.StoreDeleteErr, mock.Anything, []interface{}{evt, cluster, sentinelErr}).Return(sentinelErr)
	}
	noStoreErr := func(evt, cluster string) {
		mockedStore.On("Delete", evt, cluster).Return(nil)
	}
	infoMock := func(evt, cluster string) {
		mockedLog.On("Info", mock.Anything, []interface{}{cluster, evt})
	}

	type test struct {
		inputEvt     string
		inputCluster string
		errMock      func(evt, cluster string)
		storeMock    func(evt, cluster string)
		logMock      func(evt, cluster string)
	}
	tests := []test{
		// Input errors
		{"", "", invalidArg, nil, nil},
		{"", "AnyCluster", invalidArg, nil, nil},
		{"AnyEvt", "", invalidArg, nil, nil},

		// Persistence errors
		{"DeleteError", "DeleteError", storeDeleteErr, deleteErr, nil},

		// Success
		{"AnyEvent", "AnyCluster", nil, noStoreErr, infoMock},
	}

	for _, test := range tests {
		if test.errMock != nil {
			test.errMock(test.inputEvt, test.inputCluster)
		}
		if test.storeMock != nil {
			test.storeMock(test.inputEvt, test.inputCluster)
		}
		if test.logMock != nil {
			test.logMock(test.inputEvt, test.inputCluster)
		}

		e := l.Unsubscribe(test.inputEvt, test.inputCluster)

		if test.errMock == nil && e != nil {
			suite.NotEqual("nil", e)
		}

		if test.logMock != nil {
			mockedLog.AssertExpectations(t)
			*mockedLog = *suite.LogMock()
		}
		if test.errMock != nil {
			mockedUtil.AssertExpectations(t)
			*mockedUtil = *suite.UtilMock()
		}
		if test.storeMock != nil {
			mockedStore.AssertExpectations(t)
			*mockedStore = mockStore{}
		}
	}
}

func TestEventChannel(t *testing.T) {
	suite := &test2.TestSuite{}
	storeInitialState := map[string][]string{"ExistentEvent": {"ExistentClusterA", "ExistentClusterB"}}

	mockedStore := &mockStore{}
	mockedLog := suite.LogMock()
	mockedUtil := suite.UtilMock()

	l, e := logic.New(logic.Props{mockedStore, mockedLog, mockedUtil})
	if e != nil {
		t.Fatal("Error creating a valid logic core in order to testUtil")
		return
	}

	sentinelErr := errors.New("used to simulate store errors")
	invalidArg := func(evt string) {
		mockedUtil.On("Err", logic.InvalidArgumentsErr, mock.Anything, []interface{}{evt}).Return(sentinelErr)
	}
	storeFetchErr := func(evt string) {
		mockedUtil.On("Err", logic.StoreFetchErr, mock.Anything, []interface{}{evt, sentinelErr}).Return(sentinelErr)
	}
	fetchErr := func(evt string) { mockedStore.On("Fetch", evt).Return(nil, sentinelErr) }
	fetchMock := func(evt string) { mockedStore.On("Fetch", evt).Return(storeInitialState[evt], nil) }

	type test struct {
		inputEvt     string
		errMock      func(evt string)
		storeMock    func(evt string)
	}
	tests := []test{
		// Input errors
		{"", invalidArg, nil},

		// Persistence errors
		{"FetchError", storeFetchErr, fetchErr},

		// Success
		{"ExistentEvent", nil, fetchMock},
	}

	for _, test := range tests {
		if test.errMock != nil {
			test.errMock(test.inputEvt)
		}
		if test.storeMock != nil {
			test.storeMock(test.inputEvt)
		}

		channel, e := l.EventChannel(test.inputEvt)

		if test.errMock == nil {
			if e != nil {
				suite.NotEqual("nil error value", e)
			}
			if channel == nil {
				suite.NotEqual("valid channel", nil)
			}
		}
		if test.errMock != nil {
			if e == nil {
				suite.NotEqual("non nil error", nil)
			}
			if channel != nil {
				suite.NotEqual("nil channel", channel)
			}
		}

		if test.errMock != nil {
			mockedUtil.AssertExpectations(t)
			*mockedUtil = *suite.UtilMock()
		}
		if test.storeMock != nil {
			mockedStore.AssertExpectations(t)
			*mockedStore = mockStore{}
		}
	}
}
