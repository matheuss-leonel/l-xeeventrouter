package logic

import "errors"

type Props struct {
	Store persistence
	Log   log
	Util  util
}

func New(p Props) (*coreLogic, error) {
	if p.Store == nil || p.Log == nil || p.Util == nil {
		return nil, errors.New("you must provide all specified props")
	}
	return &coreLogic{p.Store,p.Log, p.Util}, nil
}
