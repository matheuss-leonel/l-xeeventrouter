package logic

type persistence interface {
	Save(evt string, cluster string) error
	Delete(evt string, cluster string) error
	Fetch(evt string) ([]string, error)
}

type util interface {
	Err(code int, msg string, i ...interface{}) error
}

type log interface {
	Error(s string, v ...interface{})
	Info(s string, v ...interface{})
	Warning(s string, v ...interface{})
}
