package repository

import (
	"sync"
)

type ram struct {
	channels map[string][]string
	mutex    *sync.Mutex
}

func (r *ram) Save(evt string, cluster string) error {
	r.mutex.Lock()
	r.channels[evt] = append(r.channels[evt], cluster)
	r.mutex.Unlock()
	return nil
}

func (r *ram) Delete(evt string, cluster string) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()

	subscribers := r.channels[evt]
	for i, clusters := range subscribers {
		if clusters == cluster {
			subscribers = append(subscribers[:i], subscribers[i+1:]...)
			r.channels[evt] = subscribers
			return nil
		}
	}
	return nil
}

func (r *ram) Fetch(evt string) ([]string, error) {
	clusters, exist := r.channels[evt]
	if !exist {
		return nil, nil
	}
	sliceCopy := make([]string, len(clusters))
	copy(sliceCopy, clusters)
	return sliceCopy, nil
}
