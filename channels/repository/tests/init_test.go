package tests

import (
	"message_broker/channels/repository"
	"testing"
)

func TestInit(t *testing.T) {
	repo := repository.NewRepository()
	if repo == nil {
		t.Fatal("This test was expected to get a valid repo struct")
	}
}
