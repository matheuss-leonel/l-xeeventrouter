package publication

import (
	"message_broker/global"
	"message_broker/util"
)

// Publish will verify the caller authenticity, and if it's a valid worker
// it will create, save and send a new event to the workers that are
// subscribing to the given event
//
// This is the only way to create a new event.
func Publish(address string, event string, data interface{}) (e *global.event) {
	// 1. Verify if the caller is a registered worker. Panic if not.
	publisher := WorkerStore.Worker(address)

	// 2. If there's no publisher registered with the given address, panic
	if publisher == nil {
		panic(util.NewErrorf("There's no Worker registered with the [%s] address", address))
		return nil
	}

	// 3. DeadShorted workers can't publish events
	if publisher.Zombie() {
		panic(util.NewErrorf("The [%s:%s] Worker can't publish events because it is DeadShorted", publisher.Name(), publisher.Address()))
		return nil
	}

	// 4. Create and save a new event for the given publisher.
	newEvt := newEvent(event, publisher.Name() + ":" + publisher.Address(), data)

	// 5. Call the notification module
	NotifyService.Notify(newEvt.Id)

	// 6. Return the published event
	return newEvt
}

