package publication

import (
	"message_broker/log"
	"message_broker/util"
	"net/http"
)

// publicationContract is the expected JSON encoded POST argument that
// a service must provide to publish any event
type publicationContract struct {
	Addr		string			`json:"address"`
	Event		string			`json:"event"`
	Data		interface{}		`json:"data"`
}

// PublishEndpoint will handle the incoming traffic for event publication.
// It expects a JSON encoded post argument that matches the publicationContract.
func PublishEndpoint(w http.ResponseWriter, r *http.Request) {
	// 1. Deserialize the POST data
	var reqArgs publicationContract
	util.Deserialize(r, &reqArgs)

	// 2. Try to publish the event (Verify caller, save event and iterate over worker instances).
	createdEvt := Publish(reqArgs.Addr, reqArgs.Event, reqArgs.Data)

	// 3. Log the event
	log.LogInfo("Event [%s] published by [%s]", createdEvt.Name, createdEvt.Publisher)
}
